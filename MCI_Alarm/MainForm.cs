﻿using MCI_Alarm.src;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Timers;

namespace MCI_Alarm
{
    public partial class MainForm : Form
    {
        SoundAlarm mSoundAlarm;        
        MciSetting mSettings;
        GetAlarmData mAlarmData = null;
        BackgroundWorker mBackgroundWorker= null;
        LineNotify mLineNotify;

        private readonly object mSoundLock = new object();

        private System.Timers.Timer mSoundTimer;

        int Index = 1;

        public MainForm()
        {
            InitializeComponent();

            this.Resize += new EventHandler(MainForm_Resize);
            frmMain_Load();
        }

        protected override void OnClosed(EventArgs e)
        {
            if (mBackgroundWorker != null && mBackgroundWorker.IsBusy)
                mBackgroundWorker.CancelAsync();

            base.OnClosed(e);
        }

        private void frmMain_Load()
        {
            var _ver = Assembly.GetExecutingAssembly().GetName().Version.ToString().Split('.');

            this.Text += " " + String.Format("{0}.{1}", _ver[0], _ver[1]);

            mSoundAlarm = new SoundAlarm();
            mLineNotify = new LineNotify();
           
            
            mSettings = new MciSetting();


            string _ViwerIniFilename = mSettings.ViwerIniFilename();
            if (String.IsNullOrEmpty(@_ViwerIniFilename) || !File.Exists(@_ViwerIniFilename))
            {
                MessageBox.Show("Please select location of WSViewerE.Ini in settings:system");
                return;
            }

            InitDB();


            //*** ListView Header
            this.listView1.Clear();
            listView1.Columns.Clear();
            listView1.HideSelection = false;
            listView1.Columns.Add("No", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("TYPE", 60, HorizontalAlignment.Center);
            listView1.Columns.Add("Status", 70, HorizontalAlignment.Center);
            listView1.Columns.Add("TIME", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("DESC", 300, HorizontalAlignment.Left);
            listView1.Columns.Add("SIG", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("Value", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("Set point", 80, HorizontalAlignment.Center);
            listView1.FullRowSelect = true;
            listView1.View = View.Details;
            listView1.Sorting = SortOrder.Descending;


            mBackgroundWorker = new BackgroundWorker();
            mBackgroundWorker.WorkerSupportsCancellation = true;
            mBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorker_DoWork);

            mBackgroundWorker.RunWorkerAsync();

        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            // int arg = (int)e.Argument;
            Dictionary<string, string> SensorType = new Dictionary<string, string>();
            Dictionary<string, TagInfo> TagInfoDict = new Dictionary<string, TagInfo>();
            DateTime DT_ToQuery = DateTime.Now;

            SensorType.Add("1", "ACC1X");
            SensorType.Add("2", "ACC1Y");
            SensorType.Add("3", "ACC1Z");
            SensorType.Add("A", "TEMP1");
            SensorType.Add("4", "ACC2X");
            SensorType.Add("5", "ACC2Y");
            SensorType.Add("6", "ACC2Z");
            SensorType.Add("B", "TEMP2");

            


            while (!worker.CancellationPending)
            {
                //bnClearEnabled(false);
                //bnConfirmEnabled(false);

                bool IsAlarm = false;

                TagInfoDict.Clear();

                var ls = new List<AlarmData>();

                List<AlarmHistory> AlmData = mAlarmData.GetData(DT_ToQuery);

                bool EnLineNotify = Properties.Settings.Default.enable_linenotify;
                string LineToken = Properties.Settings.Default.linenotify_token;

                foreach (var item in AlmData)
                {
                    TagInfo Sensor;

                    if (TagInfoDict.ContainsKey(item.TAGID) == false )
                    {
                        Sensor = mAlarmData.GetTagInfo(item.TAGID);

                        TagInfoDict.Add(item.TAGID, Sensor);                    }
                    else
                    {
                        Sensor = TagInfoDict[item.TAGID];
                    }

                    String ProbeName = "";
                    if( item.SCLASS.ToUpper() == "1" ||
                        item.SCLASS.ToUpper() == "2" ||
                        item.SCLASS.ToUpper() == "3" ||
                        item.SCLASS.ToUpper() == "A" )
                    {
                        ProbeName = Sensor.Sensor1Name;
                    }
                    else if (item.SCLASS.ToUpper() == "4" ||
                        item.SCLASS.ToUpper() == "5" ||
                        item.SCLASS.ToUpper() == "6" ||
                        item.SCLASS.ToUpper() == "B")
                    {
                        ProbeName = Sensor.Sensor2Name;
                    }
                    else
                    {
                        continue;
                    }

                    DT_ToQuery = item.ALMTIME;
                    String Desc = Sensor.Name + " : " + ProbeName;
                    ls.Add(  new AlarmData { NO = Index.ToString("D3"), 
                                             TYPE = SensorType[item.SCLASS.ToUpper()], 
                                             STS = item.ACLASS == "1" ? "Alarm" : "Normal", 
                                             TIME = item.ALMTIME.ToString("dd/MM/yyyy HH:mm:ss"),
                                             DESC = Sensor.Name + " : " + ProbeName, 
                                             SIG = item.SIG,  
                                             PV  = item.PV.ToString(),
                                             SV  = item.SV.ToString()});;

                    if (item.ACLASS == "1")
                    {
                        IsAlarm = true;
                    }

                    if (EnLineNotify)
                    {
                        mLineNotify.Send(LineToken, Desc + " " + SensorType[item.SCLASS.ToUpper()] + " " + (item.ACLASS == "1" ? "สถานะ ผิดปกติ" : "สถานะ ปกติ"));
                    }

                    Index++;
                }



                foreach (var item in ls)
                {
                    var lvi = new ListViewItem(item.NO);


                    lvi.UseItemStyleForSubItems = false;
                    lvi.SubItems.Add(item.TYPE);
                    lvi.SubItems.Add(item.STS);
                    lvi.SubItems.Add(item.TIME);
                    lvi.SubItems.Add(item.DESC);
                    lvi.SubItems.Add(item.SIG);
                    lvi.SubItems.Add(item.PV);
                    lvi.SubItems.Add(item.SV);
                    if (item.STS == "Alarm")
                    {
                        lvi.SubItems[2].ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        lvi.SubItems[2].ForeColor = System.Drawing.Color.Green;
                    }
                    listViewAddItem(lvi);
                }

                

                bool EnSound = Properties.Settings.Default.enable_sound;
                if (EnSound && IsAlarm == true)
                {
                    lock (mSoundLock)
                    {
                        if (mSoundTimer != null)
                        {
                            mSoundTimer.Stop();
                            mSoundTimer = null;
                        }

                        string sound_filename = Properties.Settings.Default.alarm_sound;
                        mSoundAlarm.Play(sound_filename);

                        var SoundPeriod_ms = GetSoundPeriod_ms();
                        if (SoundPeriod_ms > 0)
                        {
                            // Create a timer with a two second interval.
                            mSoundTimer = new System.Timers.Timer(SoundPeriod_ms);
                            // Hook up the Elapsed event for the timer. 
                            mSoundTimer.Elapsed += OnStopSoundTimer;
                            mSoundTimer.AutoReset = false;
                            mSoundTimer.Enabled = true;
                        }
                    }
                }

                if(IsAlarm)
                {
                    FormShow();
                }

                //bnClearEnabled(true);
                //bnConfirmEnabled(true);

                Thread.Sleep(15000);

                //worker.ReportProgress(0, "AN OBJECT TO PASS TO THE UI-THREAD");
            }


        }

        private int GetSoundPeriod_ms()
        {

            var str = Properties.Settings.Default.sound_period;
            int Period_ms = 0;
            if (str == "Always-On")
            {
                Period_ms = 0;
            }else if(str == "30 sec")
            {
                Period_ms = 30 * 1000;
            }
            else if(str == "1 min")
            {
                Period_ms = 1 * 60 * 1000;
            }
            else if(str == "3 min")
            {
                Period_ms = 30 * 60 * 1000;
            }

            return Period_ms;
        }

        private void OnStopSoundTimer(Object source, ElapsedEventArgs e)
        {
            lock (mSoundLock)
            {
                if (mSoundTimer != null)
                {
                    mSoundTimer.Stop();
                    mSoundTimer = null;
                }

                mSoundAlarm.Stop();

            }
        }


        private void InitDB()
        {

            List<String>  Driver = new List<String>();

            // Get the HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers key.
            RegistryKey keyLocalMachine =
                Registry.LocalMachine.OpenSubKey(@"SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers", false);
            string[] valueNames = keyLocalMachine.GetValueNames();
            for (int i = 0; i < valueNames.Length; i++)
                Driver.Add(valueNames[i]);


            String  DBType = mSettings.GetDBType().ToUpper();
            if (DBType == "JET")
            {
                bool detected_Driver = false;
                // Ms Access
                foreach(String s in Driver)
                {
                    if(s == "Microsoft Access Driver(*.mdb, *.accdb)")
                    {
                        detected_Driver = true;
                        break;
                    }
                }

                if(!detected_Driver)
                {
                    MessageBox.Show("Please install Microsoft Access Driver(*.mdb, *.accdb) driver");
                    Close();
                }

                String param = "Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq={0};Uid={1};Pwd={2};";
                param = param.Replace("{0}", @mSettings.GetDBServerAddress() + mSettings.GetMDBName());
                param = param.Replace("{1}", mSettings.GetDBUserName());
                param = param.Replace("{2}", mSettings.GetDBPassword());

                GetAlarmData_MSAccess _mAlarmData = new GetAlarmData_MSAccess(mSettings);
                _mAlarmData.Init(param);
                mAlarmData = _mAlarmData;
            }else if (DBType == "SQLSERVER")
            {
                bool detected_Driver = false;
                // Ms Access
                foreach (String s in Driver)
                {
                    if ( s.IndexOf("ODBC Driver 17 for SQL Server") == 0)
                    {
                        detected_Driver = true;
                        break;
                    }
                }

                if (!detected_Driver)
                {
                    MessageBox.Show("Please install ODBC Driver 17 for SQL Server");
                    Close();
                }

                String param = "Driver={ODBC Driver 17 for SQL Server};Server={0};Database={1};UID={2};PWD={3};";
                param = param.Replace("{0}", @mSettings.GetDBServerAddress());
                param = param.Replace("{1}", mSettings.GetMDBName());
                param = param.Replace("{2}", mSettings.GetDBUserName());
                param = param.Replace("{3}", mSettings.GetDBPassword());

                GetAlarmData_MSSQL _mAlarmData = new GetAlarmData_MSSQL(mSettings);
                _mAlarmData.Init(param);
                mAlarmData = _mAlarmData;
            }
            else
            {
                MessageBox.Show("Database type " + DBType + " is not supported yet!" );
            }
        }



        private void bnStopBZ_Click(object sender, EventArgs e)
        {
            lock (mSoundLock)
            {
                if (mSoundTimer != null)
                {
                    mSoundTimer.Stop();
                    mSoundTimer = null;
                }

                mSoundAlarm.Stop();
                this.pBalarm.Image = global::MCI_Alarm._.led_grey;
                //LineNotify("EPYAWd6C4rlYk5NRJzjtRBynkdBEts8jybiOvo4S6u6", "Sensor1 alarm");
            }

        }

        private void bnSystem_Click(object sender, EventArgs e)
        {
            
            PasswordForm frm = new PasswordForm();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                SystemForm sfrm = new SystemForm(mSettings);
                sfrm.ShowDialog();
            }

        }

        private void bnUser_Click(object sender, EventArgs e)
        {
            UserForm frm = new UserForm(mSettings);
            frm.ShowDialog();
        }

        private async void bnClear_Click(object sender, EventArgs e)
        {
            bnClear.Enabled = false;
            await Task.Run(() =>
            {
                listViewClear();                
            });

            bnClear.Enabled = true;
            bnClear.Focus();
        }

        private void listViewClear()
        {
            if (listView1.InvokeRequired)
            {
                // Call this same method but append THREAD2 to the text
                Action safelistViewClear = delegate { listViewClear(); };
                listView1.Invoke(safelistViewClear);
            }
            else
            {
                listView1.Items.Clear();
                Index = 1;
            }
        }

        private void listViewAddItem(ListViewItem lvi)
        {
            if (listView1.InvokeRequired)
            {
                Action safelistViewAddItem = delegate { listViewAddItem(lvi); };
                listView1.Invoke(safelistViewAddItem);
            }
            else
            {
                listView1.Items.Add(lvi);
            }
        }

        private void bnClearEnabled(bool enable)
        {
            if (bnClear.InvokeRequired)
            {
                Action fn = delegate { bnClearEnabled(enable); };
                bnClear.Invoke(fn);
            }
            else
            {
                bnClear.Enabled = enable;
            }
        }

        private void bnConfirmEnabled(bool enable)
        {
            
            if (bnConfirm.InvokeRequired)
            {
                Action fn = delegate { bnConfirmEnabled(enable); };
                bnConfirm.Invoke(fn);
            }
            else
            {
                bnConfirm.Enabled = enable;
            }
        }

        private void FormShow()
        {
            if (this.InvokeRequired)
            {
                Action fn = delegate { FormShow(); };
                this.Invoke(fn);
            }
            else
            {
                Show();
                this.WindowState = FormWindowState.Normal;
                notifyIcon1.Visible = false;
                this.pBalarm.Image = global::MCI_Alarm._.led_red;
            }
        }

        private void listViewConfirm()
        {
            if (listView1.InvokeRequired)
            {
                Action fn = delegate { listViewConfirm(); };
                listView1.Invoke(fn);
            }
            else
            {
                foreach (ListViewItem lvi in listView1.Items)
                {
                    lvi.UseItemStyleForSubItems = true;
                    lvi.SubItems[0].BackColor = DefaultBackColor;

                }
            }
        }

        private async void bnConfirm_Click(object sender, EventArgs e)
        {
            bnConfirm.Enabled = false;
            await Task.Run(() =>
            {
                listViewConfirm();
            });

            bnConfirm.Enabled = true;
            bnConfirm.Focus();
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            //if the form is minimized  
            //hide it from the task bar  
            //and show the system tray icon (represented by the NotifyIcon control)  
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
                notifyIcon1.Visible = true;

                notifyIcon1.BalloonTipText = " ";
                notifyIcon1.BalloonTipTitle = notifyIcon1.Text + " is running in background";
                notifyIcon1.ShowBalloonTip(500);
            }

        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            FormShow();
        }


    }
}
