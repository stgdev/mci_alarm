﻿
using System.Windows.Forms;

namespace MCI_Alarm
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bnStopBZ = new System.Windows.Forms.Button();
            this.bnClear = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.bnConfirm = new System.Windows.Forms.Button();
            this.bnUser = new System.Windows.Forms.Button();
            this.bnSystem = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.pBalarm = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pBalarm)).BeginInit();
            this.SuspendLayout();
            // 
            // bnStopBZ
            // 
            this.bnStopBZ.Location = new System.Drawing.Point(12, 12);
            this.bnStopBZ.Name = "bnStopBZ";
            this.bnStopBZ.Size = new System.Drawing.Size(122, 29);
            this.bnStopBZ.TabIndex = 0;
            this.bnStopBZ.Text = "Stop Alram";
            this.bnStopBZ.UseVisualStyleBackColor = true;
            this.bnStopBZ.Click += new System.EventHandler(this.bnStopBZ_Click);
            // 
            // bnClear
            // 
            this.bnClear.Location = new System.Drawing.Point(268, 12);
            this.bnClear.Name = "bnClear";
            this.bnClear.Size = new System.Drawing.Size(122, 29);
            this.bnClear.TabIndex = 1;
            this.bnClear.Text = "Clear";
            this.bnClear.UseVisualStyleBackColor = true;
            this.bnClear.Click += new System.EventHandler(this.bnClear_Click);
            // 
            // listView1
            // 
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView1.Location = new System.Drawing.Point(13, 47);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(957, 494);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // bnConfirm
            // 
            this.bnConfirm.Location = new System.Drawing.Point(140, 12);
            this.bnConfirm.Name = "bnConfirm";
            this.bnConfirm.Size = new System.Drawing.Size(122, 29);
            this.bnConfirm.TabIndex = 3;
            this.bnConfirm.Text = "Confirm";
            this.bnConfirm.UseVisualStyleBackColor = true;
            this.bnConfirm.Click += new System.EventHandler(this.bnConfirm_Click);
            // 
            // bnUser
            // 
            this.bnUser.Location = new System.Drawing.Point(720, 12);
            this.bnUser.Name = "bnUser";
            this.bnUser.Size = new System.Drawing.Size(122, 29);
            this.bnUser.TabIndex = 4;
            this.bnUser.Text = "User";
            this.bnUser.UseVisualStyleBackColor = true;
            this.bnUser.Click += new System.EventHandler(this.bnUser_Click);
            // 
            // bnSystem
            // 
            this.bnSystem.Location = new System.Drawing.Point(848, 12);
            this.bnSystem.Name = "bnSystem";
            this.bnSystem.Size = new System.Drawing.Size(122, 29);
            this.bnSystem.TabIndex = 5;
            this.bnSystem.Text = "System";
            this.bnSystem.UseVisualStyleBackColor = true;
            this.bnSystem.Click += new System.EventHandler(this.bnSystem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(649, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Settings:";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "MCI Alarm";
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // pBalarm
            // 
            this.pBalarm.Image = global::MCI_Alarm._.led_grey;
            this.pBalarm.Location = new System.Drawing.Point(396, 12);
            this.pBalarm.Name = "pBalarm";
            this.pBalarm.Size = new System.Drawing.Size(30, 30);
            this.pBalarm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBalarm.TabIndex = 7;
            this.pBalarm.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(982, 553);
            this.Controls.Add(this.pBalarm);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bnSystem);
            this.Controls.Add(this.bnUser);
            this.Controls.Add(this.bnConfirm);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.bnClear);
            this.Controls.Add(this.bnStopBZ);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MCI Alarm";
            ((System.ComponentModel.ISupportInitialize)(this.pBalarm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button bnStopBZ;
        private Button bnClear;
        private ListView listView1;
        private Button bnConfirm;
        private Button bnUser;
        private Button bnSystem;
        private Label label1;
        private NotifyIcon notifyIcon1;
        private PictureBox pBalarm;
    }
}

