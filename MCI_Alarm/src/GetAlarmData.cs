
using System.Net;
using System.IO;
using System.Text;
using System.Data.Odbc;
using System;
using System.Collections.Generic;
using MCI_Alarm.src;

public class AlarmData
{
    public string NO { get; set; }
    public string TYPE { get; set; }
    public string STS { get; set; }
    public string TIME { get; set; }
    public string DESC { get; set; }
    public string SIG { get; set; }
    public string PV { get; set; }
    public string SV { get; set; }
}


public class AlarmHistory
{
    public DateTime ALMTIME { get; set; }
    public string TAGID { get; set; }
    public string SCLASS { get; set; }
    public string ACLASS { get; set; }
    public string SIG { get; set; }
    public float PV { get; set; }
    public float SV { get; set; }
    public int MAIL { get; set; }
}

public class TagInfo
{
    public string Name { get; set; }
    public string Sensor1Name { get; set; }
    public string Sensor2Name { get; set; }
}

public class GetAlarmData
{
    protected MciSetting  mEfjSetting;

    public GetAlarmData(MciSetting Settings)
    {
        mEfjSetting = Settings;
    }
    
    public TagInfo GetTagInfo(String id)
    {
        return Impl_GetTagInfo(id);
    }

    protected virtual TagInfo Impl_GetTagInfo(String id)
    {
        return null;
    }

    public List<AlarmHistory> GetData(DateTime dt = default(DateTime))
    {
        List<AlarmHistory> History = Impl_GetData(dt);

        //List<AlarmData> datatable = new List<AlarmData>();

        return History;
    }

    public virtual List<AlarmHistory> Impl_GetData(DateTime dt)
    {
        return new List<AlarmHistory>();
    }
}