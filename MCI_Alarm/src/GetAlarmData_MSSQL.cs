﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCI_Alarm.src
{
    public class GetAlarmData_MSSQL : GetAlarmData
    {

         private String mConnString = String.Empty;
         OdbcConnection mCnn = null;

        public GetAlarmData_MSSQL(MciSetting mciSetting) : base(mciSetting)
        {

        }

        public void Init(String ConnString)
        {
            mConnString = ConnString;
        }

        private bool Connect()
        {
            bool isOK = false;

            mCnn = new OdbcConnection(mConnString);
            try
            {
                mCnn.Open();
                isOK = true;
            }
            catch
            {
                MessageBox.Show("Connection Failed ! " + mConnString);
                isOK = false;
            }

            return isOK;
        }

        private void Close()
        {
            if (mCnn != null)
                mCnn.Close();
        }


        protected override TagInfo Impl_GetTagInfo(String id)
        {
            Connect();

            TagInfo taginfo = new TagInfo();

            String tagmaster = mEfjSetting.VIWER_INI().GetData("DB", "TAGMASTER");

            String cmd = "SELECT * FROM {0} WHERE TAGID='?'".Replace("{0}", tagmaster).Replace("?", id);
            OdbcCommand MyCommand = new OdbcCommand(cmd, mCnn);

            OdbcDataReader myReader;
            myReader = MyCommand.ExecuteReader();

            if (myReader.Read())
            {
                taginfo.Name = myReader.GetString(3);
                taginfo.Sensor1Name = myReader.GetString(4);
                taginfo.Sensor2Name = myReader.GetString(5);
            }

            Close();

            return taginfo;
        }


        public override List<AlarmHistory> Impl_GetData(DateTime dt)
        {

            Connect();

            List<AlarmHistory> Data = new List<AlarmHistory>();

            String almhistory = mEfjSetting.VIWER_INI().GetData("ALARM", "ALMHIST");


            String cmd = "SELECT * FROM {0} {where} ORDER BY ALMTIME ASC".Replace("{0}", almhistory);

            if (dt == default(DateTime))
            {
                cmd = cmd.Replace("{where}", "");
            }
            else
            {
                cmd = cmd.Replace("{where}", "WHERE ALMTIME > " + "CAST('" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "' as datetime)");
            }
            

            OdbcCommand MyCommand = new OdbcCommand(cmd, mCnn);

            OdbcDataReader myReader;
            myReader = MyCommand.ExecuteReader();

            while (myReader.Read())
            {

                try
                {
                    Data.Add(new AlarmHistory
                    {
                        ALMTIME = myReader.GetDateTime(0),
                        TAGID = myReader.GetString(1),
                        SCLASS = myReader.GetString(2),
                        ACLASS = myReader.GetString(3),
                        SIG = myReader.GetString(4),
                        PV = myReader.GetFloat(5),
                        SV = myReader.GetFloat(6),
                        MAIL = myReader.GetInt16(7)
                    });
                }
                catch
                {
                    // Skipped

                }

            }

            Close();

            return Data;
        }

    }

    
}
