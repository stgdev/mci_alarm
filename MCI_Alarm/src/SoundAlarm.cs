﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCI_Alarm.src
{
    public class SoundAlarm
    {
        System.Media.SoundPlayer mPlayer = null;

        ~SoundAlarm()
        {
            if (mPlayer != null)
            {
                mPlayer.Stop();
            }
        }
        public void Play(String filename)
        {
            if (mPlayer == null )
            {
                mPlayer = new System.Media.SoundPlayer(@filename);
            }
            else
            {
                mPlayer.Stop();
            }

            mPlayer.PlayLooping();
        }
        public void Stop()
        {
            if (mPlayer != null )
            {
                mPlayer.Stop();
            }
        }

    }
}
