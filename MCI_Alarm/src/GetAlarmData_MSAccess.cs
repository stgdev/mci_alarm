
using System.Net;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System;
using System.Collections.Generic;
using MCI_Alarm.src;
using System.Windows.Forms;
//using System.Data.Odbc;

public class GetAlarmData_MSAccess : GetAlarmData
{

    private String mConnString = String.Empty;
    OdbcConnection mCnn = null;

    public GetAlarmData_MSAccess(MciSetting mciSetting):base(mciSetting)
    {
        
    }

    ~GetAlarmData_MSAccess()
    {
    }
    
    public void Init(String ConnString)
    {
        mConnString = ConnString;
    }

    private bool Connect()
    {
            bool isOK = false;
            
            //connetionString = "Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=C:\\www\\posassist-node\\static\\XDBDATA_0A9A_21032020_huge.mdb;Uid=;Pwd=;";
            mCnn = new OdbcConnection(mConnString);
            try
            {
                mCnn.Open();
                isOK = true;
            }
            catch 
            {
                MessageBox.Show("Connection Failed ! "  + mConnString);
                isOK = false;
            }

        return isOK;
    }
    
    private void Close()
    {
        if ( mCnn != null )
            mCnn.Close();
    }


    protected override TagInfo Impl_GetTagInfo(String id)
    {        
        Connect();

        TagInfo taginfo = new TagInfo();

        //DataSet ds = new DataSet();

        String tagmaster = mEfjSetting.VIWER_INI().GetData("DB","TAGMASTER");

        OdbcCommand MyCommand = new OdbcCommand("SELECT * FROM {0} WHERE TAGID=?".Replace("{0}", tagmaster), mCnn);
        MyCommand.Parameters.Add("?", OdbcType.Text).Value = id; 

        OdbcDataReader myReader;
        myReader = MyCommand.ExecuteReader();

        if(myReader.Read())
        {            
            taginfo.Name = myReader.GetString(3);
            taginfo.Sensor1Name =  myReader.GetString(4);
            taginfo.Sensor2Name =  myReader.GetString(5);
        }

        Close();

        return taginfo;
    }


    public override List<AlarmHistory> Impl_GetData(DateTime dt)
    {

        Connect();

        List<AlarmHistory> Data = new List<AlarmHistory>();

        String almhistory = mEfjSetting.VIWER_INI().GetData("ALARM", "ALMHIST");

        String cmd = "SELECT * FROM {0} {where} ORDER BY ALMTIME ASC".Replace("{0}", almhistory);

        if(dt == default(DateTime))
        {
            cmd = cmd.Replace("{where}", "");
        }else
        {
            cmd = cmd.Replace("{where}", "WHERE ALMTIME > " + "#" +dt.ToString("MM/dd/yyyy HH:mm:ss") + "#");
        }

        OdbcCommand MyCommand = new OdbcCommand(cmd, mCnn);
        //OdbcCommand MyCommand = new OdbcCommand("SELECT * FROM ALMHISTORY WHERE ALMTIME >= ?", mCnn)
        //MyCommand.Parameters.Add("?", OdbcType.Text).Value = id; 

        OdbcDataReader myReader;
        myReader = MyCommand.ExecuteReader();

        while(myReader.Read())
        {

            try
            {
                Data.Add(new AlarmHistory { ALMTIME     = myReader.GetDateTime(0), 
                                            TAGID       = myReader.GetString(1), 
                                            SCLASS      = myReader.GetString(2), 
                                            ACLASS      = myReader.GetString(3), 
                                            SIG         = myReader.GetString(4),
                                            PV          = myReader.GetFloat(5),
                                            SV          = myReader.GetFloat(6),
                                            MAIL        = myReader.GetInt16(7)                                      
                                            });
            }catch
            {
                // Skipped
                
            }
        
        }

        Close();

        return Data;
    }

}