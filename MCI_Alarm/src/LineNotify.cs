﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MCI_Alarm.src
{
    class LineNotify
    {        

        public async void Send(string lineToken, string message = "", int stickerPackageID = 0, int stickerID = 0, string pictureUrl = "")
        {
            try
            {
                using var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + lineToken);
                string _message = System.Web.HttpUtility.UrlEncode(message, Encoding.UTF8);
                var postData = string.Format("message={0}", _message);
                if (stickerPackageID > 0 && stickerID > 0)
                {
                    var stickerPackageId = string.Format("stickerPackageId={0}", stickerPackageID);
                    var stickerId = string.Format("stickerId={0}", stickerID);
                    postData += "&" + stickerPackageId.ToString() + "&" + stickerId.ToString();
                }
                if (pictureUrl != "")
                {
                    var imageThumbnail = string.Format("imageThumbnail={0}", pictureUrl);
                    var imageFullsize = string.Format("imageFullsize={0}", pictureUrl);
                    postData += "&" + imageThumbnail.ToString() + "&" + imageFullsize.ToString();
                }

                var data = new StringContent(postData, Encoding.UTF8, "application/x-www-form-urlencoded");

                var response = await client.PostAsync("https://notify-api.line.me/api/notify", data);

                string result = response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}
