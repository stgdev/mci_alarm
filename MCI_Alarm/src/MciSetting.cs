﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IniParser;
using IniParser.Model;

namespace MCI_Alarm.src
{
    public class MciSetting
    {
        IniHandle mViwerIni;

        public MciSetting()
        {
            mViwerIni = new IniHandle();

            var _ViwerIniFilename = Properties.Settings.Default.viewer_ini;

            if (!String.IsNullOrEmpty(@_ViwerIniFilename) && File.Exists(@_ViwerIniFilename))
            {
                mViwerIni.SetFilename(@_ViwerIniFilename);
            }
        }

        public void SetViwerIniFilename(String iniFilename)
        {
            Properties.Settings.Default.viewer_ini = iniFilename;
            Properties.Settings.Default.Save();
            mViwerIni.SetFilename(iniFilename);
        }
        public String ViwerIniFilename()
        {
            return Properties.Settings.Default.viewer_ini;
        }

        public IniHandle VIWER_INI()
        {
            return mViwerIni;
        }

        public String GetDBType()
        {            
            return mViwerIni.GetData("DB", "DBTYPE");
        }

        public String GetMDBName()
        {
            return mViwerIni.GetData("DB", "MDBNAME");
        }

        public String GetDBServerAddress()
        {
            return mViwerIni.GetData("DB", "SVRADDR");
        }

        public String GetDBUserName()
        {
            return mViwerIni.GetData("DB", "USERID");
        }
        public String GetDBPassword()
        {
            return mViwerIni.GetData("DB", "PASSWORD");
        }
    }

    public class IniHandle
    {
        protected FileIniDataParser mParser;
        protected String iniFn;

        public IniHandle(String iniFilename = null)
        {
            mParser = new FileIniDataParser();
            SetFilename(iniFilename);
        }

        public void SetFilename(String iniFilename)
        {
            iniFn = iniFilename;
        }

        public String GetData(String section, String Key)
        {
            IniData data = mParser.ReadFile(iniFn);
            return String.IsNullOrEmpty(data[section][Key]) ? String.Empty : data[section][Key];
        }

        public void UpdateData(String section, String Key, String Value)
        {
            IniData data = mParser.ReadFile(iniFn);
            data[section][Key] = Value;
            mParser.WriteFile(iniFn, data);
        }
    }

    public class ConfigurationIniHandle : IniHandle
    {
        public ConfigurationIniHandle():base("Configuration.ini")
        {            

            if (!File.Exists(iniFn))
            {
                var f = File.Create(iniFn);
                f.Close();

                IniData data = mParser.ReadFile(iniFn);
                data["VIEWER_INI"]["filename"] = "";
                mParser.WriteFile(iniFn, data);
            }
        }
    }
}
