﻿using MCI_Alarm.src;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCI_Alarm
{
    public partial class UserForm : Form
    {
        MciSetting mEfjSetting;
        SoundAlarm mSoundAlarm;

        public UserForm(MciSetting efjSetting)
        {
            InitializeComponent();

            mSoundAlarm = new SoundAlarm();
            mEfjSetting = efjSetting;

            bool EnSound = Properties.Settings.Default.enable_sound;
            bool EnLineNotify = Properties.Settings.Default.enable_linenotify;

            textBoxLineToken.Text = Properties.Settings.Default.linenotify_token;

            checkBoxEnSound.Checked = EnSound;
            checkBoxEnLine.Checked  = EnLineNotify;

            bnBrowseSound.Enabled       = checkBoxEnSound.Checked;
            bnTestSound.Enabled         = checkBoxEnSound.Checked;
            cbSdPeriod.Enabled          = checkBoxEnSound.Checked;
            bnTestNotify.Enabled        = checkBoxEnLine.Checked;
            textBoxLineToken.Enabled    = checkBoxEnLine.Checked;


            string sound_filename = Properties.Settings.Default.alarm_sound;

            if (String.IsNullOrEmpty(sound_filename))
            {
                sound_filename = mEfjSetting.VIWER_INI().GetData("ALARM", "SNDFILE");

                string currentDirectory = Path.GetDirectoryName(mEfjSetting.ViwerIniFilename());
                string FullPath = Path.GetFullPath(currentDirectory);

                sound_filename = @FullPath + Path.DirectorySeparatorChar + sound_filename;
                Properties.Settings.Default.alarm_sound = sound_filename;
                Properties.Settings.Default.Save();

            }

            textBoxSoundFilename.Text = sound_filename;


            string sound_period = Properties.Settings.Default.sound_period;
            if (!String.IsNullOrEmpty(sound_period))
            {
                cbSdPeriod.Text = sound_period;
            }

        }

        private void bnOK_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.sound_period = cbSdPeriod.Text;
            Properties.Settings.Default.linenotify_token = textBoxLineToken.Text;
            Properties.Settings.Default.Save();
            Close();
        }

        private void bnBrowseSound_Click(object sender, EventArgs e)
        {
            openFileDialogSound.InitialDirectory = "c:\\";
            openFileDialogSound.Filter = "wav files (*.wav)|*.wav|All files (*.*)|*.*";
            openFileDialogSound.FilterIndex = 0;
            openFileDialogSound.RestoreDirectory = true;
            openFileDialogSound.FileName = "siren01.wav";


            
            if (!String.IsNullOrEmpty(@textBoxSoundFilename.Text) && File.Exists(@textBoxSoundFilename.Text))
            {
                string currentDirectory = Path.GetDirectoryName(@textBoxSoundFilename.Text);

                string FullPath = Path.GetFullPath(currentDirectory);

                openFileDialogSound.InitialDirectory = FullPath;
                openFileDialogSound.FileName = Path.GetFileName(@textBoxSoundFilename.Text);
            }
            
            
            if (openFileDialogSound.ShowDialog() == DialogResult.OK)
            {
                textBoxSoundFilename.Text = openFileDialogSound.FileName;
                Properties.Settings.Default.alarm_sound = textBoxSoundFilename.Text;
                Properties.Settings.Default.Save();
            }
        }

        private void checkBoxEnSound_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEnSound.Checked)
            {
                bnBrowseSound.Enabled = true;
                bnTestSound.Enabled = true;
                cbSdPeriod.Enabled = true;
                Properties.Settings.Default.enable_sound = true;
            }
            else
            {
                bnBrowseSound.Enabled = false;
                bnTestSound.Enabled = false;
                cbSdPeriod.Enabled = false;
                Properties.Settings.Default.enable_sound = false;
                mSoundAlarm.Stop();
                bnTestSound.Text = "Play";
            }

            Properties.Settings.Default.Save();
        }

        private void checkBoxEnLine_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEnLine.Checked)
            {
                bnTestNotify.Enabled = true;
                textBoxLineToken.Enabled = true;
                Properties.Settings.Default.enable_linenotify = true;
            }
            else
            {
                bnTestNotify.Enabled = false;
                textBoxLineToken.Enabled = false;
                Properties.Settings.Default.enable_linenotify = false;
            }

            Properties.Settings.Default.Save();
        }

        private void bnTestNotify_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.linenotify_token = textBoxLineToken.Text;
            Properties.Settings.Default.Save();
            var _LineNotify = new LineNotify();
            _LineNotify.Send(textBoxLineToken.Text, "Test Notify");
        }

        private void bnTestSound_Click(object sender, EventArgs e)
        {
            if (bnTestSound.Text == "Play" )
            {                
                mSoundAlarm.Play(@textBoxSoundFilename.Text);
                bnTestSound.Text = "Stop";
            }
            else
            {
                mSoundAlarm.Stop();
                bnTestSound.Text = "Play";
            }
        }

        private void cbSdPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(!Properties.Settings.Default.enable_sound)
            {
                return;
            }
        }
    }
}
