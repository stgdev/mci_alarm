﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCI_Alarm
{
    public partial class PasswordForm : Form
    {
        public PasswordForm()
        {
            InitializeComponent();
            labelInfo.Visible = false;
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bnOK_Click(object sender, EventArgs e)
        {
            if (textBoxPw.Text == "mcialarm")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                labelInfo.Visible = true;
            }
        }

        private void textBoxPw_TextChanged(object sender, EventArgs e)
        {
            labelInfo.Visible = false;
        }

        private void textBoxPw_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.bnOK.PerformClick();
            }
        }
    }
}
