﻿
namespace MCI_Alarm
{
    partial class UserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxLineToken = new System.Windows.Forms.TextBox();
            this.checkBoxEnLine = new System.Windows.Forms.CheckBox();
            this.groupBoxAlarm = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bnTestNotify = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbSdPeriod = new System.Windows.Forms.ComboBox();
            this.checkBoxEnSound = new System.Windows.Forms.CheckBox();
            this.bnTestSound = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSoundFilename = new System.Windows.Forms.TextBox();
            this.bnBrowseSound = new System.Windows.Forms.Button();
            this.bnOK = new System.Windows.Forms.Button();
            this.openFileDialogSound = new System.Windows.Forms.OpenFileDialog();
            this.groupBoxAlarm.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Token : ";
            // 
            // textBoxLineToken
            // 
            this.textBoxLineToken.Location = new System.Drawing.Point(74, 59);
            this.textBoxLineToken.Name = "textBoxLineToken";
            this.textBoxLineToken.Size = new System.Drawing.Size(467, 27);
            this.textBoxLineToken.TabIndex = 1;
            // 
            // checkBoxEnLine
            // 
            this.checkBoxEnLine.AutoSize = true;
            this.checkBoxEnLine.Location = new System.Drawing.Point(9, 26);
            this.checkBoxEnLine.Name = "checkBoxEnLine";
            this.checkBoxEnLine.Size = new System.Drawing.Size(155, 24);
            this.checkBoxEnLine.TabIndex = 2;
            this.checkBoxEnLine.Text = "Enable LINE Notify";
            this.checkBoxEnLine.UseVisualStyleBackColor = true;
            this.checkBoxEnLine.CheckedChanged += new System.EventHandler(this.checkBoxEnLine_CheckedChanged);
            // 
            // groupBoxAlarm
            // 
            this.groupBoxAlarm.Controls.Add(this.groupBox2);
            this.groupBoxAlarm.Controls.Add(this.groupBox1);
            this.groupBoxAlarm.Location = new System.Drawing.Point(12, 12);
            this.groupBoxAlarm.Name = "groupBoxAlarm";
            this.groupBoxAlarm.Size = new System.Drawing.Size(580, 391);
            this.groupBoxAlarm.TabIndex = 4;
            this.groupBoxAlarm.TabStop = false;
            this.groupBoxAlarm.Text = "Alarm";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxEnLine);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.bnTestNotify);
            this.groupBox2.Controls.Add(this.textBoxLineToken);
            this.groupBox2.Location = new System.Drawing.Point(21, 246);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(547, 124);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "LINE Notify";
            // 
            // bnTestNotify
            // 
            this.bnTestNotify.Location = new System.Drawing.Point(447, 92);
            this.bnTestNotify.Name = "bnTestNotify";
            this.bnTestNotify.Size = new System.Drawing.Size(94, 29);
            this.bnTestNotify.TabIndex = 6;
            this.bnTestNotify.Text = "Test Notify";
            this.bnTestNotify.UseVisualStyleBackColor = true;
            this.bnTestNotify.Click += new System.EventHandler(this.bnTestNotify_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbSdPeriod);
            this.groupBox1.Controls.Add(this.checkBoxEnSound);
            this.groupBox1.Controls.Add(this.bnTestSound);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxSoundFilename);
            this.groupBox1.Controls.Add(this.bnBrowseSound);
            this.groupBox1.Location = new System.Drawing.Point(21, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(553, 200);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sound";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Period";
            // 
            // cbSdPeriod
            // 
            this.cbSdPeriod.FormattingEnabled = true;
            this.cbSdPeriod.Items.AddRange(new object[] {
            "Always-On",
            "30 sec",
            "1 min",
            "3 min"});
            this.cbSdPeriod.Location = new System.Drawing.Point(68, 149);
            this.cbSdPeriod.Name = "cbSdPeriod";
            this.cbSdPeriod.Size = new System.Drawing.Size(151, 28);
            this.cbSdPeriod.TabIndex = 9;
            this.cbSdPeriod.SelectedIndexChanged += new System.EventHandler(this.cbSdPeriod_SelectedIndexChanged);
            // 
            // checkBoxEnSound
            // 
            this.checkBoxEnSound.AutoSize = true;
            this.checkBoxEnSound.Location = new System.Drawing.Point(9, 26);
            this.checkBoxEnSound.Name = "checkBoxEnSound";
            this.checkBoxEnSound.Size = new System.Drawing.Size(76, 24);
            this.checkBoxEnSound.TabIndex = 3;
            this.checkBoxEnSound.Text = "Enable";
            this.checkBoxEnSound.UseVisualStyleBackColor = true;
            this.checkBoxEnSound.CheckedChanged += new System.EventHandler(this.checkBoxEnSound_CheckedChanged);
            // 
            // bnTestSound
            // 
            this.bnTestSound.Location = new System.Drawing.Point(353, 96);
            this.bnTestSound.Name = "bnTestSound";
            this.bnTestSound.Size = new System.Drawing.Size(94, 29);
            this.bnTestSound.TabIndex = 8;
            this.bnTestSound.Text = "Play";
            this.bnTestSound.UseVisualStyleBackColor = true;
            this.bnTestSound.Click += new System.EventHandler(this.bnTestSound_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "File";
            // 
            // textBoxSoundFilename
            // 
            this.textBoxSoundFilename.Location = new System.Drawing.Point(61, 59);
            this.textBoxSoundFilename.Name = "textBoxSoundFilename";
            this.textBoxSoundFilename.ReadOnly = true;
            this.textBoxSoundFilename.Size = new System.Drawing.Size(486, 27);
            this.textBoxSoundFilename.TabIndex = 6;
            // 
            // bnBrowseSound
            // 
            this.bnBrowseSound.Location = new System.Drawing.Point(453, 96);
            this.bnBrowseSound.Name = "bnBrowseSound";
            this.bnBrowseSound.Size = new System.Drawing.Size(94, 29);
            this.bnBrowseSound.TabIndex = 7;
            this.bnBrowseSound.Text = "browse";
            this.bnBrowseSound.UseVisualStyleBackColor = true;
            this.bnBrowseSound.Click += new System.EventHandler(this.bnBrowseSound_Click);
            // 
            // bnOK
            // 
            this.bnOK.Location = new System.Drawing.Point(492, 409);
            this.bnOK.Name = "bnOK";
            this.bnOK.Size = new System.Drawing.Size(94, 29);
            this.bnOK.TabIndex = 5;
            this.bnOK.Text = "OK";
            this.bnOK.UseVisualStyleBackColor = true;
            this.bnOK.Click += new System.EventHandler(this.bnOK_Click);
            // 
            // openFileDialogSound
            // 
            this.openFileDialogSound.FileName = "openFileDialogSound";
            // 
            // UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 450);
            this.ControlBox = false;
            this.Controls.Add(this.bnOK);
            this.Controls.Add(this.groupBoxAlarm);
            this.Name = "UserForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "User Settings";
            this.groupBoxAlarm.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxLineToken;
        private System.Windows.Forms.CheckBox checkBoxEnLine;
        private System.Windows.Forms.GroupBox groupBoxAlarm;
        private System.Windows.Forms.Button bnOK;
        private System.Windows.Forms.OpenFileDialog openFileDialogSound;
        private System.Windows.Forms.Button bnTestNotify;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbSdPeriod;
        private System.Windows.Forms.CheckBox checkBoxEnSound;
        private System.Windows.Forms.Button bnTestSound;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxSoundFilename;
        private System.Windows.Forms.Button bnBrowseSound;
    }
}