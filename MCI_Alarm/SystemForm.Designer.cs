﻿
using System.Windows.Forms;

namespace MCI_Alarm
{
    partial class SystemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialogViewerINI = new System.Windows.Forms.OpenFileDialog();
            this.bnBrowse = new System.Windows.Forms.Button();
            this.textBoxViwerINI = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bnOK = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "INI File";
            // 
            // openFileDialogViewerINI
            // 
            this.openFileDialogViewerINI.FileName = "openFileDialog";
            // 
            // bnBrowse
            // 
            this.bnBrowse.Location = new System.Drawing.Point(408, 59);
            this.bnBrowse.Name = "bnBrowse";
            this.bnBrowse.Size = new System.Drawing.Size(94, 29);
            this.bnBrowse.TabIndex = 1;
            this.bnBrowse.Text = "browse";
            this.bnBrowse.UseVisualStyleBackColor = true;
            this.bnBrowse.Click += new System.EventHandler(this.bnBrowse_Click);
            // 
            // textBoxViwerINI
            // 
            this.textBoxViwerINI.Location = new System.Drawing.Point(67, 26);
            this.textBoxViwerINI.Name = "textBoxViwerINI";
            this.textBoxViwerINI.ReadOnly = true;
            this.textBoxViwerINI.Size = new System.Drawing.Size(435, 27);
            this.textBoxViwerINI.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.bnBrowse);
            this.groupBox1.Controls.Add(this.textBoxViwerINI);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(516, 114);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Viewer";
            // 
            // bnOK
            // 
            this.bnOK.Location = new System.Drawing.Point(420, 409);
            this.bnOK.Name = "bnOK";
            this.bnOK.Size = new System.Drawing.Size(94, 29);
            this.bnOK.TabIndex = 4;
            this.bnOK.Text = "OK";
            this.bnOK.UseVisualStyleBackColor = true;
            this.bnOK.Click += new System.EventHandler(this.bnOK_Click);
            // 
            // SystemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 450);
            this.ControlBox = false;
            this.Controls.Add(this.bnOK);
            this.Controls.Add(this.groupBox1);
            this.Name = "SystemForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "System Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label label1;
        private OpenFileDialog openFileDialogViewerINI;
        private Button bnBrowse;
        private TextBox textBoxViwerINI;
        private GroupBox groupBox1;
        private Button bnOK;
    }
}