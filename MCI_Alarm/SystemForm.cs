﻿using MCI_Alarm.src;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCI_Alarm
{
    public partial class SystemForm : Form
    {
        MciSetting mEfjSetting;
        public SystemForm(MciSetting efjSetting)
        {
            InitializeComponent();
            mEfjSetting = efjSetting;

            string ViwerIniFilename = mEfjSetting.ViwerIniFilename();

            if (!String.IsNullOrEmpty(ViwerIniFilename) && File.Exists(ViwerIniFilename))
            {
                textBoxViwerINI.Text = ViwerIniFilename;
            }
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialogViewerINI.InitialDirectory = "c:\\";
            openFileDialogViewerINI.Filter = "ini files (*.ini)|*.ini|All files (*.*)|*.*";
            openFileDialogViewerINI.FilterIndex = 0;
            openFileDialogViewerINI.RestoreDirectory = true;
            openFileDialogViewerINI.FileName = "WSViewerE.Ini";

            string ViwerIniFilename = mEfjSetting.ViwerIniFilename();

            if (!String.IsNullOrEmpty(ViwerIniFilename) && File.Exists(ViwerIniFilename))
            {
                string currentDirectory = Path.GetDirectoryName(ViwerIniFilename);

                openFileDialogViewerINI.InitialDirectory = Path.GetFullPath(currentDirectory);

                openFileDialogViewerINI.FileName = ViwerIniFilename;
            }

            if (openFileDialogViewerINI.ShowDialog() == DialogResult.OK)
            {
                textBoxViwerINI.Text = @openFileDialogViewerINI.FileName;
                mEfjSetting.SetViwerIniFilename(@openFileDialogViewerINI.FileName);
            }
        }

        private void bnOK_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
